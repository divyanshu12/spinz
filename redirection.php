<?php
    include_once 'connection.php';
if(!empty($_POST))
{
    $name   = $_POST['name'];
    $mobile = $_POST['phone'];
    $current_date   = date("Y-m-d");
    if(!empty($mobile) && $mobile != '')
    {
        $sql    = mysqli_query($conn,"INSERT INTO `daily_quiz_db`.`tbl_spinz_details` (`fullname`,`mobile`,`created_date`) VALUES ('".$name."','".$mobile."','".$current_date."') ") ;
    }
}
mysqli_close($conn);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Buddy Loan</title>
    <link href="https://pagecdn.io/lib/easyfonts/istok-web.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Halant&family=Inter&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
</head>

<body style="background: #F4F4F4;">
    <div class="container">
        <table class="table table-bordered" id="main-table" style="background: #fff;">
            <tr>
                <td>
                    <img class="logo" src="assets/images/logo.png" alt="Quiz Logo">
                </td>
                <td>
                    <p class="title">
                        <span class="title-bold">Earn&nbsp;upto&nbsp;<span style="font-size: 25px;">₹</span><b>80</b></span>
                        by Spinning a Wheel!
                    </p>
                </td>
            </tr>
        </table>
        <section class="redirect-page mb-3" style="text-align:center;margin-top: 68px;">
            <div>
                <h2 style="color: #47B6F2;font-weight:bold">Hello !!</h2>
                <h5 style="font-weight:500;letter-spacing:1px">Redeem the Cash Quickly</h5>
            </div>
            <div class="buddyloan-logo">
                <img src="assets/images/buddyloanlogo.png" alt="buddyloan logo">
            </div>
            <div class="mb-2">
                <h4 style="font-weight:600;">Redeem ₹50 Cash</h4>
                <p style="color: #4D4D4D;font-size:16px;line-height: 23px;margin-top: 18px;">Download the Buddy Loan App Now<br />
                    and Get the Cash Instantly in your Account</p>
            </div>
            <div class="mt-4">
                <button class="btn download-btn" id="download-btn">Download Now</button>
            </div>
        </section>
    </div>
    <script>
        $('#download-btn').click(function() {
            setTimeout(() => {
                window.location.href = 'https://play.google.com/store/apps/details?id=com.buddyloan.vls&referrer=utm_source%3DNat_Redirect%26utm_medium%3DPrelander';
            },5000);
           
        })
    </script>
</body>

</html>